screen _ml_mod_settings tag smallscreen2:
    modal True

    window id "_ml_mod_settings" at popup2:
        style "smallwindow"

        add "image/ui/settings_title.png" xalign 0.01 yalign 0.1 at title_slide
        
        if _ml_screen_nav:
            imagebutton idle "image/ui/close_idle.png" hover "image/ui/close_hover.png" action [Hide("_ml_mod_settings"), Show("preferences_nav"), Play("audio", "se/sounds/close.ogg")] hovered Play("audio", "se/sounds/select.ogg") style "smallwindowclose" at nav_button
        else:
            imagebutton idle "image/ui/close_idle.png" hover "image/ui/close_hover.png" action [Hide("_ml_mod_settings"), Show("preferences"), Play("audio", "se/sounds/close.ogg")] hovered Play("audio", "se/sounds/select.ogg") style "smallwindowclose" at nav_button
