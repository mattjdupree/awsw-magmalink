from modloader import modast
from renpy import ast
from renpy.exports import error

import jz_magmalink.ast_link.utils as utils

class Searchable:
    def _search(self, condition, depth, error_msg):
        return utils._search(self.node, condition, depth, error_msg)
    
    def search_say(self, text=None, depth=200):
        if text is None:
            return self._search(ast.Say, depth, "Cannot find say statement.")
        return self._search(lambda n: isinstance(n, ast.Say) and n.what.startswith(text), depth, "Cannot find say statement with text: " + text)
    
    def search_scene(self, image=None, depth=200):
        if image is None:
            return self._search(ast.Scene, depth, "Cannot find scene statement.")
        return self._search(lambda n: isinstance(n, ast.Scene) and n.imspec is not None and ' '.join(n.imspec[0]) == image, depth, "Cannot find scene statement with image: " + image)
    
    def search_show(self, image=None, depth=200):
        if image is None:
            return self._search(ast.Show, depth, "Cannot find show statement.")
        return self._search(lambda n: isinstance(n, ast.Show) and ' '.join(n.imspec[0]) == image, depth, "Cannot find show statement with image: " + image)
    
    def search_hide(self, image=None, depth=200):
        if image is None:
            return self._search(ast.Hide, depth, "Cannot find hide statement.")
        return self._search(lambda n: isinstance(n, ast.Hide) and ' '.join(n.imspec[0]) == image, depth, "Cannot find hide statement with image: " + image)
    
    def search_menu(self, menu_items=None, depth=200):
        if menu_items is None:
            return self._search(ast.Menu, depth, "Cannot find menu.")
        if isinstance(menu_items, unicode) or isinstance(menu_items, str):
            menu_items = [menu_items]
        found = self._search(lambda n: isinstance(n, ast.Menu) and all((e in (i[0] for i in n.items) for e in menu_items)), depth, "Cannot find menu with items: " + str(menu_items))
        found.needle = menu_items[0]
        return found
    
    def search_if(self, condition=None, depth=200):
        if condition is None:
            return self._search(ast.If, depth, "Cannot find if statement.")
        found = self._search(lambda n: isinstance(n, ast.If) and any((e[0] == condition for e in n.entries)), depth, "Cannot find if statement with condition: " + condition)
        found.needle = condition
        return found
    
    def search_python(self, code=None, depth=200):
        if code is None:
            return self._search(ast.Python, depth, "Cannot find python statement.")
        return self._search(lambda n: isinstance(n, ast.Python) and n.code.source == code, depth, "Cannot find python statement with code: " + code)
    
    def search_call(self, label_name=None, depth=200):
        if label_name is None:
            return self._search(ast.Call, depth, "Cannot find call statement.")
        return self._search(lambda n: isinstance(n, ast.Call) and n.label == label_name, depth, "Cannot find call statement with label: " + label_name)
    
    def search_with(self, expression=None, depth=200):
        if expression is None:
            return self._search(ast.With, depth, "Cannot find with statement.")
        return self._search(lambda n: isinstance(n, ast.With) and n.expr is not None and n.expr == expression, depth, "Cannot find with statement with expression: " + expression)
    
    def search_play(self, filename=None, depth=200):
        if filename is None:
            return self._search(lambda n: isinstance(n, ast.UserStatement) and n.line.startswith("play "), depth, "Cannot find play statement.")
        return self._search(lambda n: isinstance(n, ast.UserStatement) and n.line.startswith("play ") and ("\"" + filename + "\"") in n.line, depth, "Cannot find play statement with filename: " + filename)
    
    def search_stop(self, depth=200):
        return self._search(lambda n: isinstance(n, ast.UserStatement) and n.line.startswith("stop "), depth, "Cannot find stop statement.")
    
    def search_window(self, depth=200):
        return self._search(lambda n: isinstance(n, ast.UserStatement) and n.line.startswith("window "), depth, "Cannot find window statement.")
    
    def search_nvl(self, depth=200):
        return self._search(lambda n: isinstance(n, ast.UserStatement) and n.line.startswith("nvl "), depth, "Cannot find nvl statement.")

class Hookable:
    def hook_to(self, label_name, condition=None, return_link=True):
        label = modast.find_label(label_name)
        if return_link and self.node.next is not None:
            utils._create_label(label_name + "_return", self.node.next)
        utils._hook(self.node, label, condition)
        return self
    
    def link_from(self, link_name):
        utils._create_label(link_name, self.node)
        return self
    
    def link_behind_from(self, link_name):
        if self.node.next is None:
            error("You cannot create a link behind the last node in the chain.")
        utils._create_label(link_name, self.node.next)
        return self
    
    def hook_from_node(self, node, condition=None):
        if isinstance(node, Hookable):
            utils._hook(node.node, self.node, condition)
        else:
            error("Node must be a hookable MagmaLink object.")
        return self
    
    def hook_to_node(self, node, condition=None):
        if isinstance(node, Hookable):
            utils._hook(self.node, node.node, condition)
        else:
            error("Node must be a hookable MagmaLink object.")
        return self
    
    def hook_call_to(self, label_name, condition=None):
        utils._hook(self.node, modast.find_label(label_name), condition, call=True)
        return self

class Node:
    def __eq__(self, obj):
        return isinstance(obj, Node) and obj.node is self.node
    
    def __ne__(self, obj):
        return not self == obj
    
    # Use only when you cannot get the next node using a search function
    def next(self):
        return utils.node(self.node.next)
    
    def edit(self, *edit_funcs):
        for edit_func in edit_funcs:
            edit_func(self.node)
        return self

class Branch(Searchable):
    def __init__(self, index, first_node, parent_node):
        self.index = index
        self.first_node = first_node
        self.parent_node = parent_node
    
    def _search(self, condition, depth, error_msg):
        return utils._search(self.first_node, condition, depth, error_msg, include_base=True)
    
    def _add_node_front(self, node):
        if isinstance(self.parent_node, ast.Menu):
            self.parent_node.items[self.index][2].insert(0, node)
        elif isinstance(self.parent_node, ast.If):
            self.parent_node.entries[self.index][1].insert(0, node)
    
    def hook_to(self, label_name, condition=None, return_link=True):
        label = modast.find_label(label_name)
        if return_link:
            utils._create_label(label_name + "_return", self.first_node)
        self._add_node_front(utils._hook(None, label, condition, self.first_node))
        return self
    
    def hook_to_node(self, node, condition=None):
        if isinstance(node, Hookable):
            self._add_node_front(utils._hook(None, node.node, condition, self.first_node))
        else:
            error("Node must be a hookable MagmaLink object.")
        return self
    
    def hook_call_to(self, label_name, condition=None):
        self._add_node_front(utils._hook(None, modast.find_label(label_name), condition, self.first_node, call=True))
        return self
    
    # Use only when you cannot get the first node using a search function
    def first(self):
        return utils.node(self.first_node)

