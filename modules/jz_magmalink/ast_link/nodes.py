from renpy.exports import error

import jz_magmalink.ast_link.base as base
import jz_magmalink.ast_link.utils as utils

class OtherNode(base.Node, base.Searchable, base.Hookable):
    def __init__(self, node):
        self.node = node

class EndNode(base.Node):
    def __init__(self, node):
        self.node = node

class MenuNode(base.Node, base.Searchable, base.Hookable):
    def __init__(self, node):
        self.node = node
        self.needle = None
        
    def get_choice(self, choice=None, with_index=False):
        if choice is None:
            choice = self.needle
        if choice is None:
            error("You have to specify what menu choice to use.")
        if with_index:
            choice_item = next(((i, o) for i, o in enumerate(self.node.items) if o[0] == choice), None)
        else:
            choice_item = next((o for o in self.node.items if o[0] == choice), None)
        if choice_item is None:
            error("Cannot find choice " + choice + " in the current menu.")
        
        return choice_item
    
    def branch(self, choice=None):
        choice = self.get_choice(choice, True)
        return base.Branch(choice[0], choice[1][2][0], self.node)
    
    def add_choice(self, text, condition="True", jump=None, before=None, after=None):
        nodes = [utils._hook(None, self.node.next if jump is None else utils._parse_jump(jump), None, self.node.next)]
        utils._add_branch_item(self.node.items, (text, condition, nodes), before, after)
        return self
    
    def edit_choice(self, choice=None, text=None, condition=None, jump=None):
        choice_index, choice_item = self.get_choice(choice, True)
        new_text = choice_item[0] if text is None else text
        new_condition = choice_item[1] if condition is None else condition
        
        new_nodes = []
        new_nodes[:] = choice_item[2]
        
        if jump is not None:
            new_nodes.insert(0, utils._hook(None, utils._parse_jump(jump), None, choice_item[2][0]))
        
        if text is None:
            self.node.items[choice_index] = (choice_item[0], new_condition, new_nodes)
        else:
            self.node.items[choice_index] = (choice_item[0], "False", new_nodes)
            new_choice = (new_text, new_condition, [utils._hook(None, new_nodes[0], None, self.node.next)])
            if choice_index == len(self.node.items) - 1:
                self.node.items.append(new_choice)
            else:
                self.node.items.insert(choice_index + 1, new_choice)
        
        return self
    
    def block_choice(self, choice=None):
        self.edit_choice(choice, condition="False")
        return self

class IfNode(base.Node, base.Searchable, base.Hookable):
    def __init__(self, node):
        self.node = node
        self.needle = None
    
    def get_entry(self, condition=None, with_index=False):
        if condition is None:
            condition = self.needle
        if condition is None:
            error("You have to specify what condition to use.")
        if with_index:
            condition_item = next(((i, o) for i, o in enumerate(self.node.entries) if o[0] == condition), None)
        else:
            condition_item = next((o for o in self.node.entries if o[0] == condition), None)
        if condition_item is None:
            error("Cannot find condition " + condition+ " in the current if statement.")
        return condition_item

    def add_entry(self, condition, jump=None, before=None, after=None):
        nodes = [utils._hook(None, self.node.next if jump is None else utils._parse_jump(jump), None, self.node.next)]
        utils._add_branch_item(self.node.entries, (condition, nodes), before, after)
        return self
    
    def add_else_entry(self, jump=None):
        if self.node.entries[-1][0] == "True":
            error("This if statement already has an \"else\" entry.")
        else:
            return self.add_entry("True", jump=jump)

    def branch(self, condition=None):
        entry = self.get_entry(condition, True)
        return base.Branch(entry[0], entry[1][1][0], self.node)
    
    def branch_else(self):
        condition_item = self.node.entries[-1]
        if condition_item[0] != "True":
            error("Cannot find else branch in the current if statement.")
        return base.Branch(len(self.node.entries) - 1, condition_item[1][0], self.node) 
