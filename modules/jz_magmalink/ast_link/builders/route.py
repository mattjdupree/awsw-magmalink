from modloader import modast
from renpy import store

import jz_magmalink.ast_link.utils as utils

class CharacterRoute:
    def __init__(self, id, name):
        self._dates = []
        self._ending = None
        self._chapter_menus = {}
        self.id = id
        self.name = name
    
    def add_date(self, text=None, condition=None, jump=None, chapters=None):
        chapters = [1, 2, 3, 4] if chapters is None else chapters
        text = ("Meet with %s."%self.name) if text is None else text
        chapter_menus = {}
        for chapter in chapters:
            if len(self._dates) >= chapter:
                continue
            if chapter not in self._chapter_menus:
                self._chapter_menus[chapter] = utils.find_character_menus(chapter)
            chapter_menus[chapter] = self._chapter_menus[chapter]
        self._dates.append((text, condition, jump, chapter_menus))
        return self
    
    def set_ending(self, text=None, condition=None, jump=None):
        self._ending = ((self.name + ".") if text is None else text, condition, jump, utils.find_ending_menu())
        return self
    
    def build(self):
        def get_hook_closure(ch, d):
            def hook_closure(h):
                modast.set_renpy_global("save_name", (store._("Chapter " + str(ch) + " - " + self.name + " " + str(d))))
                modast.set_renpy_global(self.id + str(d) + "_unplayed", False)
                modast.set_renpy_global(self.id + "_ch" + str(ch) + "_available", False)
                return False
            return hook_closure
        
        condition = self.id + "1_unplayed"
        for i, date in enumerate(self._dates):
            modast.set_renpy_global(self.id + str(i+1) + "_unplayed", True)
            node_to = utils._parse_jump(date[2])
            
            for chapter, menus in date[3].items():
                modast.set_renpy_global(self.id + "_ch" + str(chapter) + "_available", True)
                
                nodes1 = [utils._create_hook(None, node_to, get_hook_closure(chapter, i+1), menus[0].node.next)]
                cond1 = condition + ("" if date[1] is None else " and (" + date[1] + ")")
                utils._add_branch_item(menus[0].node.items, (date[0], cond1, nodes1), None, None)
                
                nodes2 = [utils._create_hook(None, node_to, get_hook_closure(chapter, i+1), menus[1].node.next)]
                cond2 = self.id + "_ch" + str(chapter) + "_available and " + cond1
                utils._add_branch_item(menus[1].node.items, (date[0], cond2, nodes2), None, None)
                    
            condition = self.id + str(i+2) + "_unplayed and not " + condition
        
        if self._ending is not None:
            def hook_closure(h):
                modast.set_renpy_global("save_name", (store._("Chapter 5 - " + self.name)))
                return False
            nodes = [utils._create_hook(None, utils._parse_jump(self._ending[2]), hook_closure, self._ending[3].node.next)]
            cond = "not " + self.id + str(len(self._dates)) + "_unplayed" + ("" if self._ending[1] is None else " and (" + self._ending[1] + ")")
            utils._add_branch_item(self._ending[3].node.items, (self._ending[0], cond, nodes), None, None)
