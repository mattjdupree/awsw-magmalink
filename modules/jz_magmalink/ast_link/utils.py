import inspect
from modloader import modast
from renpy import ast, python, game
from renpy.game import script
from renpy.exports import error

def _next(node):
    if isinstance(node, modast.ASTHook):
        return node.old_next
    else:
        return node.next

def _search(base_node, condition, depth, error_msg, include_base=False):
    for i in range(0, depth):
        if i > 0 or not include_base:
            base_node = _next(base_node)
        if base_node is None:
            error(error_msg)
        if (inspect.isclass(condition) and isinstance(base_node, condition)) or (not inspect.isclass(condition) and condition(base_node)):
            return node(base_node)
    
    error(error_msg)

def _hook(node_from=None, node_to=None, condition=None, old_next=None, call=False):
    if node_to is None:
        error("`node_to` cannot be None")
    
    is_call = call and isinstance(node_to, ast.Label)
    if condition is None:
        if is_call:
            def call_closure(hook):
                label = game.context().call(node_to.name, return_site=hook.old_next.name)
                hook.chain(label)
                ast.next_node(label)
                return True
                
            return _create_hook(node_from=node_from, func=call_closure, old_next=old_next)
        else:
            return _create_hook(node_from=node_from, node_to=node_to, old_next=old_next)
    else:
        if not isinstance(condition, (str, unicode)):
            error("`condition` keyword must be a string containing executable python code. Instead got: %r"%condition)

        def condition_closure(hook):
            # By default, avoid mod code
            ast.next_node(hook.old_next)

            # If the condition is true, jump to the label
            if python.py_eval(condition):
                n = node_to
                if is_call:
                    label = game.context().call(n.name, return_site=hook.old_next.name)
                    hook.chain(label)
                    n = label
                ast.next_node(n)
                
            # Return a "truthy" value to tell the modtools not to set the next_node itself.
            return True

        # We don't re-chain this by default because we want it to be completely ignore-able if something goes wrong.
        return _create_hook(node_from=node_from, func=condition_closure, old_next=old_next)

def _create_label(label_name, next_node):
    if script.has_label(label_name):
        error("Cannot create label, '" + label_name + "' already exists.")
    new_label = ast.Label(("MagmaLink", 1), label_name, [], None)
    new_label.next = next_node
    script.namemap[label_name] = new_label

def _create_hook(node_from=None, node_to=None, func=None, old_next=None, tag=None):
    if tag:
        tag = "ml_core_"+tag
    hook = modast.ASTHook(("MagmaLink", 1), func, node_from, tag=tag)
    if node_from is not None:
        if old_next is None:
            old_next = node_from.next
        
        if isinstance(node_from, ast.Label):
            node_from.next = hook
        else:
            node_from.chain(hook)
    
    hook.old_next = old_next
    if node_to is None:
        hook.chain(old_next)
    else:
        hook.chain(node_to)
    
    return hook

def _parse_jump(jump):
    import jz_magmalink.ast_link.base as base
    
    if isinstance(jump, base.Node):
        node = jump.node
    elif isinstance(jump, ast.Node):
        node = jump
    elif isinstance(jump, (str, unicode)):
        node = modast.find_label(jump)
    else:
        error("Invalid value for the \"jump\" parameter.")
    
    return node

def _add_branch_item(items, new_item, before, after):
    if before is None and after is None:
        items.append(new_item)
        return
    elif before is not None and after is not None:
        error("Cannot use \"before\" and \"after\" simultaneously.")

    index = -1
    items.append(items[-1])
    for i in range(len(items) - 1, 0, -1):
        items[i] = items[i-1]
        if before is not None and items[i][0] == before:
            index = i-1
            break
        if after is not None and items[i][0] == after:
            index = i
            break
    items[index] = new_item

def node(n):
    import jz_magmalink.ast_link.nodes as nodes
    
    if isinstance(n, ast.Menu):
        return nodes.MenuNode(n)
    elif isinstance(n, ast.If):
        return nodes.IfNode(n)
    elif isinstance(n, (ast.Jump, ast.Return)):
        return nodes.EndNode(n)
    elif isinstance(n, ast.Node):
        return nodes.OtherNode(n)
    else:
        error("Object of type " + type(n).__name__ + " isn't a node.")

def find_label(label_name):
    return node(modast.find_label(label_name))

def find_character_menus(chapters=None, variant=None):
    if chapters is None:
        chapters = [1, 2, 3, 4]
    elif isinstance(chapters, int):
        chapters = [chapters]
    
    variants = [1, 2] if variant is None else [variant]
    menus = []
    
    if 1 in chapters:
        c = find_label("chapter1chars").search_if("chapter1csplayed == 0")
        if 1 in variants:
            menus.append(c.branch().search_menu())
        if 2 in variants:
            menus.append(c.branch("chapter1csplayed == 1").search_menu())
    if 2 in chapters:
        c = find_label("chapter2chars").search_python("adineavailable = False").search_if("chapter2csplayed == 0")
        if 1 in variants:
            menus.append(c.branch().search_menu())
        if 2 in variants:
            menus.append(c.branch("chapter2csplayed == 1").search_menu())
    if 3 in chapters:
        if 1 in variants:
            menus.append(find_label('chapter3chars2').search_menu())
        if 2 in variants:
            menus.append(find_label('chapter3chars3').search_menu())
    if 4 in chapters:
        if 1 in variants:
            menus.append(find_label('chapter4chars2').search_menu())
        if 2 in variants:
            menus.append(find_label('chapter4chars3').search_menu())
    
    return menus

def find_ending_menu():
    return find_label("chapter5").search_menu()
