from renpy import ast, exports, config, python, game
from renpy.exports import error, display_menu

import jz_magmalink.ast_link.utils as utils

def _paginate_store_menu(choices):
    if len(choices) == 0:
        error("Cannot paginate an empty menu.")
    if any((value is None for _,value in choices)):
        error("Cannot paginate a menu with a None value.")

    if len(choices) > 7:
        n = 5
        rv = "NEXTPAGE"
        while rv == "NEXTPAGE":
            for choicepage in [choices[i:i+n] for i in range(0, len(choices), n)]:
                choicepage.append(("[[Show more options.]", "NEXTPAGE"))
                rv = display_menu(choicepage)
                if rv != "NEXTPAGE":
                    break
    else:
        rv = display_menu(choices)

    return rv

def _show_charmenu(calling_node):
    ast.next_node(calling_node.next)
    ast.statement_name("magmalink_paginated_menu")
    
    menu = utils.node(calling_node).search_menu()
    choices = []

    for i, (label, condition, block) in enumerate(menu.node.items):
        if config.say_menu_text_filter:
            label = config.say_menu_text_filter(label)
        
        if block is None:
            error("Angels with Scaly Wings character menus should not have menu labels without blocks.")
        else:
            if python.py_eval(condition):
                choices.append(((label % exports.tag_quoting_dict) if config.old_substitutions else label, i))

    ast.say_menu_with(menu.node.with_, game.interface.set_transition)

    if menu.node.set:
        set = python.py_eval(menu.node.set)
        choices = [(label, value) for label,value in choices if label not in set]
    else:
        set = None

    if not choices:
        return None

    choice = _paginate_store_menu(choices)

    if set is not None and choice is not None:
        for label, value in choices:
            if value == choice:
                try:
                    set.append(label)
                except AttributeError:
                    set.add(label)
                break

    if choice is not None:
        ast.next_node(menu.node.items[choice][2][0])

    return True

def _link_charmenuframework():
    c1csplayed = utils.find_label("chapter1chars") \
        .search_if("chapter1csplayed == 0")

    c2csplayed = utils.find_label("chapter2chars") \
        .search_if("playmessage == True") \
        .search_if("chapter2csplayed == 0")

    cpaginationifs = [
        ("c1pm1",c1csplayed.branch().search_say("(What should I do?)")),
        ("c1pm2",c1csplayed.branch('chapter1csplayed == 1').search_say("(More free time. What should I do?)")),
        ("c2pm1",c2csplayed.branch().search_if('chapter2count >= 7')),
        ("c2pm2",c2csplayed.branch('chapter2csplayed == 1').search_if('chapter2count >= 7')),
        ("c3pm1",utils.find_label('chapter3chars2').search_if('chapter3count >= 7')),
        ("c3pm2",utils.find_label('chapter3chars3').search_if('chapter3count >= 7')),
        ("c4pm1",utils.find_label('chapter4chars2').search_if('chapter4count >= 7')),
        ("c4pm2",utils.find_label('chapter4chars3').search_if('chapter4count >= 7')),
    ]

    for tag, paginationif in cpaginationifs:
        if isinstance(paginationif.node, ast.If):
            # In all chapters except Chapter1,
            #  need to disable Saunders' pagination.
            paginationif.node.entries = []
        
        utils._create_hook(node_from=paginationif.node, func=_show_charmenu, tag=tag)

    c5menutrailer = utils.find_label('chapter5') \
        .search_say("(Today is the day of the big fireworks. Who shall I bring?)") \
        ._search(lambda n: isinstance(utils._next(n), ast.Menu), 50, "Chapter 5 character menu not within 50 nodes of 'if loremdead == False'")

    utils._create_hook(node_from=c5menutrailer.node, func=_show_charmenu, tag="c5pm") 
