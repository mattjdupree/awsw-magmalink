from modloader import modast

import jz_magmalink.sl_link.utils as utils
import jz_magmalink.sl_link.builders.overlay as overlay

_mod_settings_btns = []

def register_mod_settings(mod, screen):
    _mod_settings_btns.append("    textbutton _(\"" + mod.name + "\") action [Hide(\"_ml_mod_settings\"), Show(\"" + screen + "\"), Play(\"audio\", \"se/sounds/open.ogg\")] hovered Play(\"audio\", \"se/sounds/select.ogg\") style \"menubutton\"")

def _create_mod_menu():
    if len(_mod_settings_btns) == 0:
        return
    
    modast.set_renpy_global("_ml_screen_nav", False)
    
    ms_button = overlay.Overlay() \
        .add("textbutton _(\"Mod settings\") action [Hide(\"preferences\"), Show(\"_ml_mod_settings\"), Play(\"audio\", \"se/sounds/open.ogg\"), SetVariable(\"_ml_screen_nav\", False)] hovered Play(\"audio\", \"se/sounds/select.ogg\") style \"menubutton\"") \
        .build()
    
    ms_button_nav = overlay.Overlay() \
        .add("textbutton _(\"Mod settings\") action [Hide(\"preferences_nav\"), Show(\"_ml_mod_settings\"), Play(\"audio\", \"se/sounds/open.ogg\"), SetVariable(\"_ml_screen_nav\", True)] hovered Play(\"audio\", \"se/sounds/select.ogg\") style \"menubutton\"") \
        .build()
        
    utils.find_screen("preferences") \
        .search_window().branch() \
        .search_box().branch() \
        .search_textbutton("textsettings") \
        .add(ms_button)
    
    utils.find_screen("preferences_nav") \
        .search_window().branch() \
        .search_box().branch() \
        .search_textbutton("textsettings_nav") \
        .add(ms_button_nav)
    
    ms_buttons = overlay.Overlay() \
        .add(["vbox xalign 0.5 yalign 0.5:", "    spacing 10"] + _mod_settings_btns) \
        .build()
    
    utils.find_screen("_ml_mod_settings") \
        .search_window().branch() \
        .search_add() \
        .add(ms_buttons)
