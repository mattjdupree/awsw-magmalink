from modloader import modast
from renpy import ast, parser

class Overlay:
    def __init__(self):
        self._tocompile = "screen dummy:\n    "
        self._default_xalign = 0
        self._default_yalign = 0
    
    def _parse_align(self, xalign, yalign):
        xalign = self._default_xalign if xalign is None else xalign
        yalign = self._default_yalign if yalign is None else yalign
        return xalign, yalign
    
    def with_align(self, xalign=None, yalign=None):
        self._default_xalign = self._default_xalign if xalign is None else xalign
        self._default_yalign = self._default_yalign if yalign is None else yalign
        return self
    
    def add_image(self, path, xalign=None, yalign=None, condition="True"):
        xalign, yalign = self._parse_align(xalign, yalign)
        return self.add("add \"" + path + "\" xalign " + str(xalign) + " yalign " + str(yalign), condition)
    
    def add_status(self, image_path, text, xalign=None, yalign=None, condition="True"):
        xalign, yalign = self._parse_align(xalign, yalign)
        return self.add([
                "vbox:",
                "    xalign " + str(xalign) + " yalign " + str(yalign),
                "    add \"" + image_path + "\" xalign 0.5 at popup_offcenter",
                "    text _(\"    " + text + "\") style \"status_text\" at popup_offcenter"
            ], condition)
    
    def add(self, source_lines, condition="True"):
        if isinstance(source_lines, (str, unicode)):
            source_lines = [source_lines]
        self._tocompile += "if " + condition + ":\n        " + "\n        ".join(source_lines) + "\n    "
        return self
    
    def build(self):
        compiled = parser.parse("FNDummy", self._tocompile)
        for node in compiled:
            if isinstance(node, ast.Init):
                return node.block[0].screen
    
    def compile_to(self, screen_name):
        modast.get_slscreen(screen_name).children.extend(self.build().children)
