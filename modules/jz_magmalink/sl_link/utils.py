from modloader import modast
from renpy.exports import error
from renpy.sl2 import slast 

def _search(parent_node, index_from, condition, name, needle, include_base=False):
    for i in range(index_from if include_base else index_from + 1, len(parent_node.children)):
        child = parent_node.children[i]
        if condition(child) and (needle is None or needle in " ".join([" ".join(kw) for kw in child.keyword])):
            return slnode(parent_node, i)
    
    if needle is None:
        error("Cannot find " + name + " displayable.")
    else:
        error("Cannot find " + name + " displayable with needle " + needle + ".")

def slnode(pn, index):
    import jz_magmalink.sl_link.base as base
    
    n = pn.children[index]
    if isinstance(n, slast.SLBlock):
        return base.SLBlock(pn, index)
    elif isinstance(n, slast.SLNode):
        return base.SLNode(pn, index)
    else:
        error("Object of type " + type(n).__name__ + " isn't a SL node.")

def find_screen(screen_name):
    import jz_magmalink.sl_link.base as base
    return base.SLBranch(modast.get_slscreen(screen_name))
