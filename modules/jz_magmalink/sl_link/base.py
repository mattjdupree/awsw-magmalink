from renpy.exports import error
from renpy.display import layout

import jz_magmalink.sl_link.utils as utils

class SLSearchable:
    def _search(self, condition, name, needle):
        return utils._search(self.parent_node, self.index, condition, name, needle)
    
    def search_window(self, needle=None):
        return self._search(lambda n: n.displayable is layout.Window, "window", needle)
    
    def search_box(self, needle=None):
        return self._search(lambda n: n.displayable is layout.MultiBox, "hbox/vbox", needle)
    
    def search_textbutton(self, needle=None):
        return self._search(lambda n: n.displayable.__name__ == "_textbutton", "textbutton", needle)
    
    def search_add(self, needle=None):
        return self._search(lambda n: n.displayable.__name__ == "sl2add", "add", needle)

class SLNode(SLSearchable):
    def __init__(self, parent_node, index):
        self.parent_node = parent_node
        self.index = index
        self.node = parent_node.children[index]
    
    def add(self, displayable):
        if self.index == len(self.parent_node.children) - 1:
            self.parent_node.children.extend(displayable.children)
        else:
            for i, child in enumerate(displayable.children):
                self.parent_node.children.insert(self.index + i + 1, child)
        return self

class SLBlock(SLNode):
    def branch(self):
        return SLBranch(self.node)

class SLBranch(SLSearchable):
    def __init__(self, parent_node):
        self.parent_node = parent_node
    
    def _search(self, condition, name, needle):
        return utils._search(self.parent_node, 0, condition, name, needle, include_base=True)
