# AWSW-MagmaLink

MagmaLink is a mod for the game Angels with Scaly Wings which serves as an expansion for Modtools, adding a more simple, convenient and effective ways for both the new and advanced modders to create their mods. 

If you're making a mod using MagmaLink, anyone playing your mod will need to have MagmaLink installed as well.

To learn more about how to make a mod using MagmaLink read the [Wiki page](https://gitlab.com/jakzie2/awsw-magmalink/-/wikis/home).
