# create a hook to this label to run code before the main menu appears
label before_main_menu:
    return

# jump to this label to jump to the main menu
label ml_main_menu:
    python:
        while renpy.call_stack_depth() > 0:
            renpy.pop_call()
    jump mainmenu

# jump to this label at the end of your custom date scene
label ml_date_end:
    if chapter4unplayed == False:
        $ c4csplayed += 1
        jump chapter4chars
    elif chapter3unplayed == False:
        $ c3csplayed += 1
        jump chapter3chars
    elif chapter2unplayed == False:
        $ chapter2csplayed += 1
        jump chapter2chars
    else:
        $ chapter1csplayed += 1
        jump chapter1chars 

# jump to this label at the end of your custom answering machine scene
label ml_answeringmachine_end:
    if chapter4unplayed == False:
        jump _ml_core_chapter4_answeringmachinehookpoint
    elif chapter3unplayed == False:
        jump _ml_core_chapter3_answeringmachinehookpoint
    elif chapter2unplayed == False:
        jump _ml_core_chapter2_answeringmachinehookpoint
    else:
        jump _ml_core_chapter1_answeringmachinehookpoint

# call this label at the end of your custom ending
label ml_ending_check(route_id, ending_type, ending_msg=None, izumi_seen=False):
    $ renpy.pause(2.0)
    $ lastendingseen = ending_type
    $ persistent.endingsseen += 1
    
    if ending_type == "good":
        $ persistent.anygoodending = True
        call optimistcheck from _call_optimistcheck_ml1
    
    if izumi_seen:
        call izumimask from _call_izumimask_ml1
        
    if not getattr(persistent, route_id + "_" + ending_type + "_ending"):
        $ setattr(persistent, route_id + "_" + ending_type + "_ending", True)
        call syscheck from _call_syscheck_ml1
        if ending_msg:
            play sound "fx/system.wav"
            s "[ending_msg]"
    
    if persistent.endingsseen == 1:
        $ persistent.firstending2 = route_id
    
    return

# ML core code
init 999 python:
    magmalink()._core_init()

label _ml_core_chapter1_messagesalready:
    $ playmessage = False
    $ popularnumber = 0
    c "(Looks like there are already some messages on the answering machine. Let's see...)"
    jump _ml_core_chapter1_messagesalready_return
